
Ansible Smarter Input
=========

An Ansible Role that processes user input with features such as default and last input values.

Requirements
------------


Role Variables
--------------

- `name`: Name of the variable to store user input. Default: `NULL`.
- `default`: Default value of the input. Exclusive with `use_last`. Default: `NULL`.
- `use_last`: Whether to use input value from the last run as default. Exclusive with `default`. Default: `False`.
- `prompt`: Optional text to use for the prompt message.

Dependencies
------------


Example Playbook
----------------

```yaml
- name: Ask for user input of 'foo'
  ansible.builtin.include_role:
    name: smarter_input
  vars:
    name: foo

- name: Ask for user input of 'foo', using input from a previous run as default
  ansible.builtin.include_role:
    name: smarter_input
  vars:
    name: foo
    use_last: True

- name: Ask for user input of 'foo', using 'bar' as default value
  ansible.builtin.include_role:
    name: smarter_input
  vars:
    name: foo
    default: bar
```

License
-------

BSD

Author Information
------------------

